<?php
/**
 * Mooncup Main template for displaying custom search
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */
?>

<form action="<?php echo home_url(); ?>" method="get" class="searchbox">
	<label for="search">Search in <?php echo home_url( '/' ); ?></label>
	<input type="text" placeholder="" name="s" id="search" value="<?php echo esc_attr( get_search_query() ); ?>" />
	<input type="hidden" name="lang" value="en">
</form>