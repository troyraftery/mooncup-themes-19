<?php
$affiliate_id  = affwp_get_affiliate_id();
$user_email    = affwp_get_affiliate_email( $affiliate_id );
$payment_email = affwp_get_affiliate_payment_email( $affiliate_id, $user_email ); // Fallback to user_email
?>

<div id="affwp-affiliate-dashboard-profile" class="affwp-tab-content">

	<h4><?php _e( 'Your Profile', 'affiliatewp' ); ?></h4>

	<form id="affwp-affiliate-dashboard-profile-form" class="affwp-form" method="post">

		<div class="affwp-wrap affwp-payment-email-wrap">
			<label for="affwp-payment-email"><?php _e( 'Your payment email address', 'affiliatewp' ); ?></label>
			<input id="affwp-payment-email" type="email" name="payment_email" value="<?php echo esc_attr( $payment_email ); ?>" />
			This is the email address that we use to pay you through Paypal. Please ensure it is correct.
		</div>

		<div class="affwp-wrap affwp-send-notifications-wrap">
			<input id="affwp-referral-notifications" type="checkbox" name="referral_notifications" value="1" <?php checked( true, get_user_meta( affwp_get_affiliate_user_id( $affiliate_id ), 'affwp_referral_notifications', true ) ); ?>/>
			<label for="affwp-referral-notifications"><?php _e( 'Enable new sales notifications', 'affiliatewp' ); ?></label>
		</div>


		<div class="affwp-save-profile-wrap">
			<input type="hidden" name="affwp_action" value="update_profile_settings" />
			<input type="hidden" name="affiliate_id" value="<?php echo absint( $affiliate_id ); ?>" />
			<input type="submit" class="button" value="<?php esc_attr_e( 'Save Changes', 'affiliatewp' ); ?>" />
			<p></p>
		</div>
		<div class="affwp-wrap">
			<p>Manage your account profile <a href="/my-account">here.</a></p>
		</div>
		<?php do_action( 'affwp_affiliate_dashboard_before_submit', $affiliate_id, affwp_get_affiliate_user_id( $affiliate_id ) ); ?>
	</form>
<p></p>
</div>
