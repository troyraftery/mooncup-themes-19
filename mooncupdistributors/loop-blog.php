<?php
/**
 * Mooncup Main template for displaying the standard Loop
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="blog-post">
		<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="blog-image-link">
			<div class="blog-image">
				
					<?php if (has_post_thumbnail( $post->ID ) ): ?>
						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
						
			        	<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>

				    <?php endif; ?>
			</div>
		</a>

			<div class="post-content">

				<div class="post-category">
						<?php

						$terms = get_the_terms( get_the_ID(), 'blogs' );
						                         
						if ( $terms && ! is_wp_error( $terms ) ) : 
						 
						    $blog_links = array('exclude' =>'129');
						 
						    foreach ( $terms as $term ) {
						        $blog_links[] = $term->name;
						    }
						                         
						    $on_blog = join( ", ", $blog_links );
						    ?>
						 
						    <span class="category-block">
						        <?php printf( esc_html__( '%s', 'textdomain' ), esc_html( $on_blog ) ); ?>
						    </span>
						<?php endif; ?>
				</div>

				<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="blog-link">
					<h1 class="post-title"><?php
						if ( is_singular() ) :
							the_title();

						else : ?>
							<?php
								the_title(); ?>
							<?php
						endif; ?>
					</h1>
				</a>
				<div class="post-meta">
					<?php mooncupmain_post_meta(); ?>
					
					<div class="post-info small caps">
						<?php the_author(); ?> / <?php echo get_the_date(); ?>
					</div>

					<div class="share-links">
						<!--<a href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Tweet this!" target="_blank"><i class="fa fa-lg fa-twitter-square"></i></a>
						<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share on Facebook" target="_blank"><i class="fa fa-lg fa-facebook-square"></i></a>
						<a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href,
	  						'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-lg fa-google-plus-square"></i></a>
	  					<a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; ?>" target="_blank"><i class="fa fa-lg fa-pinterest-square"></i></a>
					-->
					</div>
				</div>

						<?php if ( is_front_page() || is_category() || is_archive() || is_search() ) : ?>
					
							<div class="excerpt"><?php the_excerpt(); ?></div>
							<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'mooncupmain' ); ?> <i class="fa fa-long-arrow-right"></i></a>

						<?php else : ?>

							<?php the_content( __( 'Continue reading &raquo', 'mooncupmain' ) ); ?>

						<?php endif; ?>

						<?php
							wp_link_pages(
								array(
									'before'           => '<div class="linked-page-nav"><p>'. __( 'This article has more parts: ', 'mooncupmain' ),
									'after'            => '</p></div>',
									'next_or_number'   => 'number',
									'separator'        => ' ',
									'pagelink'         => __( '&lt;%&gt;', 'mooncupmain' ),
								)
							);
						?>
				
			</div>
	</div>

</article>
