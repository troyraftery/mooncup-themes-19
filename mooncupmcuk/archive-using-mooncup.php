<?php
/**
 * Mooncup Main template for displaying Archives
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */

get_header(); ?>
<section class="using-your-mooncup using-mooncup-category page-content primary" role="main">
		
	       <article class="container_full splash-content-block">
	        	<?php if (has_post_thumbnail( $post->ID ) ): ?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php echo $image[0]; ?>');">
		        <?php endif; ?>
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<h1><?php _e('All Questions','mooncupmain'); ?></h1>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        <section class="container_boxed content_band">
	        	<aside class="sidebar col__4">
	        		<ul class="sidebar"><?php
						if ( function_exists( 'dynamic_sidebar' ) ) :
							dynamic_sidebar( 'faq-sidebar' );
						endif; ?>
					</ul>	
	        	</aside>

	        	<article class="faq-content faq-content-archive col__8">
	        	
				    <?php
				
					if ( have_posts() ) : ?>

						<h1 class="archive-title">
							All Questions
							<!--<?php
								if ( is_category() ):
									printf( __( 'Category Archives: %s', 'mooncupmain' ), single_cat_title( '', false ) );

								elseif ( is_tag() ):
									printf( __( 'Tag Archives: %s', 'mooncupmain' ), single_tag_title( '', false ) );

								elseif ( is_tax() ):
									$term     = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
									$taxonomy = get_taxonomy( get_query_var( 'taxonomy' ) );
									printf( __( '%s Archives: %s', 'mooncupmain' ), $taxonomy->labels->singular_name, $term->name );

								elseif ( is_day() ) :
									printf( __( 'Daily Archives: %s', 'mooncupmain' ), get_the_date() );

								elseif ( is_month() ) :
									printf( __( 'Monthly Archives: %s', 'mooncupmain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'mooncupmain' ) ) );

								elseif ( is_year() ) :
									printf( __( 'Yearly Archives: %s', 'mooncupmain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'mooncupmain' ) ) );

								elseif ( is_author() ) : the_post();
									printf( __( 'All posts by %s', 'mooncupmain' ), get_the_author() );

								else :
									_e( 'Archives', 'mooncupmain' );

								endif;
							?>-->
						</h1><?php

						if ( is_category() || is_tag() || is_tax() ):
							$term_description = term_description();
							if ( ! empty( $term_description ) ) : ?>

								<div class="archive-description"><?php
									echo $term_description; ?>
								</div><?php

							endif;
						endif;

						if ( is_author() && get_the_author_meta( 'description' ) ) : ?>

							<div class="archive-description">
								<?php the_author_meta( 'description' ); ?>
							</div>

							
							<?php endif;?>
						
					<div class="faq-item-listing">
						<!--<?php wp_list_categories(); ?>-->

						<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'loop', 'usage' );

						endwhile;

					else :

						get_template_part( 'loop', 'empty' );

					endif; ?>
					</div>

					<div class="pagination">

						<?php get_template_part( 'template-part', 'pagination' ); ?>

					</div>
	        	</article>

	        </section>
	
</section>

<?php get_footer(); ?>