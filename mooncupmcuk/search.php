<?php
/**
 * Mooncup Main template for displaying Search-Results-Pages
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */

get_header(); ?>

	<section class="custom-search page-content primary" role="main">
		<div class="container_boxed"><?php

		if ( have_posts() ) : ?>

			<div class="search-title">
				<h1 ><?php printf( __( 'Search Results for: %s', 'mooncupmain' ), get_search_query() ); ?></h1>

				<div class="second-search">
					<p>
						<?php _e( 'Not what you searched for? Try again with some different keywords.', 'mooncupmain' ); ?>
					</p>

					<?php get_search_form(); ?>
				</div>
			</div>

			<div class="container_full"><?php

			while ( have_posts() ) : the_post();

				get_template_part( 'loop', get_post_format() );

				wp_link_pages(
					array(
						'before'           => '<div class="linked-page-nav"><p>' . sprintf( __( '<em>%s</em> is separated in multiple parts:', 'mooncupmain' ), get_the_title() ) . '<br />',
						'after'            => '</p></div>',
						'next_or_number'   => 'number',
						'separator'        => ' ',
						'pagelink'         => __( '&raquo; Part %', 'mooncupmain' ),
					)
				);

			endwhile;

			else :

				get_template_part( 'loop', 'empty' );

			endif; ?>

			<div class="pagination">

				<?php get_template_part( 'template-part', 'pagination' ); ?>

			</div>
		</div>
	</div>
	</section>

<?php get_footer(); ?>